class ProductCategoryController < ApplicationController	
	protect_from_forgery with: :null_session

	def list
		@product_category = ProductCategory.all
		respond_to do |format|
      		format.html
      		format.json { render json: @product_category }
    	end
	end

	def create
		@product_category = ProductCategory.new
		@product_category.name = params[:name]
		if @product_category.save
			respond_to do |format|
      			format.json { render json: @product_category ,status: :ok }
      		end
		else
      		format.json { render json: { "status" => false, "message" => "No result" }, status: :bad_request }
      	end
	end

	def update
		if ProductCategory.update(params[:id],:name => params[:name])
      		respond_to do |format|
      			format.json { render json: ProductCategory.find(params[:id]) ,status: :ok }
      		end
      	else
      		format.json { render json: { "status" => false, "message" => "No result" }, status: :bad_request }
      	end
	end
end
