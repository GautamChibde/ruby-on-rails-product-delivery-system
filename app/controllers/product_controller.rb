class ProductController < ApplicationController
	protect_from_forgery with: :null_session

	def index
		@product = Product.find_by_sql(
			"SELECT 
				pd.id,
				pd.name,
				pd.product_detail,
				cast(row_to_json(pc) AS jsonb) AS category 
			FROM products AS pd 
				LEFT JOIN 
					product_categories AS pc 
						ON pd.product_category_id = pc.id")
		respond_to do |format|
      		format.html
      		format.json { render json: @product }
    	end
	end

	def edit
		@product = Product.find(params[:id])
    	if @product.update_attributes(params)
			format.html
      		format.json { render json: @product, status: :ok }      	
      	else
      		format.json { render json: { "status" => false, "message" => "No result" }, status: :ok }
      	end
	end

	def create
		
	end
end