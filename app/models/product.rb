class Product < ApplicationRecord
	scope :finished, -> { 
    	where("products.product_detail->>'finished' = :true", true: "true") 
  	}
	belongs_to :product_category
	validates :name, presence: true
end
