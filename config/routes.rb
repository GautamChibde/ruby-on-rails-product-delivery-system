Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  	get 'product_category' => 'product_category#list' , :defaults => { :format => 'json' }
    post 'product_category' => 'product_category#create' , :defaults => { :format => 'json' }
    put 'product_category' => 'product_category#update' , :defaults => { :format => 'json' }

    get 'product' => 'product#index' , :defaults => { :format => 'json' }
    put 'product' => 'product#edit' , :defaults => { :format => 'json' }
end
